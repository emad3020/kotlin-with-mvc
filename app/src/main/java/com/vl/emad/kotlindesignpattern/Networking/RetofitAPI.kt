package com.vl.emad.kotlindesignpattern.Networking

import com.vl.emad.kotlindesignpattern.Common.API_KEY
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetofitAPI {


    @GET("services/rest/")
    fun search(@Query("method") method: String,
               @Query("text") searchTag: String,
               @Query("page") pageIndex: Int,
               @Query("api_key") apikey: String = API_KEY,
               @Query("format") responseFormatting : String ="json",
               @Query("nojsoncallback") jsonCalls : Int = 1) : Call<ResponseBody>


    companion object  Factory{
        fun create() : RetofitAPI {
            return RetofitClient.getInstance()!!.create(RetofitAPI::class.java)
        }
    }
}