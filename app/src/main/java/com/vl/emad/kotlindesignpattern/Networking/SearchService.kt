package com.vl.emad.kotlindesignpattern.Networking

import com.vl.emad.kotlindesignpattern.Common.QUERY_SEARCH
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object SearchService {

    var mApi: RetofitAPI? = null
    fun startSearch(keyword : String, index : Int , completion: (Boolean) -> Unit) {

        mApi = RetofitAPI.create()


        mApi!!.search(QUERY_SEARCH,keyword,index).enqueue( object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    completion(true)
                } else {
                    completion(false)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                completion(false)
            }

        })
    }
}