package com.vl.emad.kotlindesignpattern

import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import com.vl.emad.kotlindesignpattern.Networking.SearchService

import com.vl.emad.kotlindesignpattern.Screens.SearchViewMvc

class MainActivity : AppCompatActivity(), SearchViewMvc.Listener {


    lateinit var viewMvc : SearchViewMvc
    var initailPage : Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewMvc = SearchViewMvc(LayoutInflater.from(this),null)

        setContentView(viewMvc.getRootView())

        viewMvc.registerListener(this@MainActivity)
    }


    override fun searchClicked(searchKey: String) {

        val progress = ProgressDialog(this)
        progress.setMessage("Loading...")
        progress.show()

        SearchService.startSearch(searchKey,initailPage){ success ->

            progress.dismiss()

            if (success) {

            } else {
                this.showToast("Can't Connect to the Internet")
            }
        }

    }

}

private fun Context.showToast(mesg : String) {

    Toast.makeText(this,mesg,Toast.LENGTH_LONG).show()
}