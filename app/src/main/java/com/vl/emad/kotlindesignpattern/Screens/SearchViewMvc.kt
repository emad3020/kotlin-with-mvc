package com.vl.emad.kotlindesignpattern.Screens

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import com.bumptech.glide.Glide.init
import com.vl.emad.kotlindesignpattern.Common.ObservableViewMvc
import com.vl.emad.kotlindesignpattern.R
import java.util.*

class SearchViewMvc(inflater: LayoutInflater, parent: ViewGroup?) : ObservableViewMvc<SearchViewMvc.Listener>{

    private var mRootView : View
    var mBtnSearch : EditText
    val mlistener  = arrayListOf<Listener>()
    init {
        mRootView = inflater.inflate(R.layout.activity_main, parent, false)
        mBtnSearch = findViewById(R.id.keywordTxt)


        mBtnSearch.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {

                    val search = mBtnSearch.text.toString()

                    if (search == null || search!!.isEmpty()) {
                        mBtnSearch.error = "Enter a key word"
                    } else {

                        for (listern in mlistener) {
                            listern.searchClicked(search)
                        }
                    }

                    true
                }

                else -> false
            }

        }
    }

    interface Listener{
        fun searchClicked(searchKey : String)
    }


    override fun registerListener(listener: Listener) {

        mlistener.add(listener)
    }

    override fun unregisterListener(listener: Listener) {

        mlistener.remove(listener)
    }

    override fun getRootView(): View {

        return mRootView
    }


    private fun <T: View> findViewById(id : Int) : T{
        return mRootView.findViewById(id)
    }

    private fun getContext() : Context {
        return mRootView.context
    }
}