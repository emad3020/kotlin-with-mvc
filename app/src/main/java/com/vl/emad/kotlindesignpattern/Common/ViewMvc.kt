package com.vl.emad.kotlindesignpattern.Common

import android.view.View

interface ViewMvc {

    fun getRootView() : View

}