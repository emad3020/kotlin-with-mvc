package com.vl.emad.kotlindesignpattern.Networking

import com.vl.emad.kotlindesignpattern.Common.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetofitClient {

     var  retofitClient : Retrofit? = null


   fun getInstance() : Retrofit? {
       if (retofitClient == null) {
           retofitClient = Retrofit.Builder()
               .addConverterFactory(GsonConverterFactory.create())
               .baseUrl(BASE_URL)
               .build();

       }

       return  retofitClient
   }
}